(function($) {

    $('#btnLoadText').click(function() { $("#showResult").load("show.txt"); });
    $('#btnAjax').click(function() { callRestAPI() });

    // Perform an asynchronous HTTP (Ajax) API request.
    function callRestAPI() {
        var root = 'https://jsonplaceholder.typicode.com';
        $.ajax({
            url: root + '/albums/33',
            method: 'GET'
        }).then(function(response) {
            console.log(response.id);
            $('#showResult').html(response.id);
        });
    }
})($);